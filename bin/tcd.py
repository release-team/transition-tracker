#!/usr/bin/python
# -*- coding: utf8 -*-
# © 2012 Cyril Brulebois <kibi@debian.org>
#
# Transition collision detector

import os
import os.path
import yaml

PACKAGES   = 'export/packages.yaml'
HTML_DIR   = 'html'
MY_EXT     = '.tcd'
MY_DIV     = 'id="tcd"'
FOOTER_DIV = 'id="footer"'


all_packages = yaml.load(open(PACKAGES, 'rb'))

# Build a package→transitions dict:
package_hits = {}
for package in all_packages:
  name = package['name']
  if not package_hits.has_key(name):
    package_hits[name] = []
  for transition in package['list']:
    if not transition[1] in ['permanent', 'finished']:
      package_hits[name].append(transition[0])

# Build a collisioner-package→transitions dict:
results = {}
for package in package_hits.keys():
  if len(package_hits[package]) > 1:
    print "Probable collision for: %s (%d)" % (package, len(package_hits[package]))
    for hit in package_hits[package]:
      print "  %s" % hit
      if not results.has_key(hit):
        results[hit] = []
      results[hit].append((package, [x for x in package_hits[package] if x != hit]))

# Post-process existing HTML files:
for html in os.listdir(HTML_DIR):
  if html.endswith('.html'):
    transition = os.path.basename(html).replace('.html', '')
    source_filename = os.path.join(HTML_DIR, html)
    dest_filename   = os.path.join(HTML_DIR, html+MY_EXT)
    source = open(source_filename, 'rb')
    dest   = open(dest_filename, 'wb')
    already_done = False
    for line in source:
      if line.find(MY_DIV) != -1:
        already_done = True
      if line.find(FOOTER_DIV) != -1 and not already_done:
        dest.write('<!-- BEGIN: Added-by: TCD -->\n')
        dest.write('<div %s><b>Collisions:</b>\n' % MY_DIV)
        if results.has_key(transition):
          dest.write('<ul>\n')
          for package, transitions in results[transition]:
            links = ['<a href="%s.html">%s</a>' % (x, x) for x in transitions]
            dest.write('<li>%s → %s</li>\n' % (package, ' & '.join(links)))
          dest.write('</ul>\n')
        else:
          dest.write('<i>none</i>\n')
        dest.write('</div>\n')
        dest.write('<!-- END: Added-by: TCD -->\n')
      dest.write(line)
    source.close()
    dest.close()
    os.remove(source_filename)
    os.rename(dest_filename, source_filename)
