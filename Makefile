BEN=/usr/bin/ben
OPT=-g /srv/release.debian.org/www/transitions/global.conf

# for custom builds of ben, please the binary and debianrt.cmxs in bin/ and set:
# BEN=/srv/release.debian.org/www/transitions/bin/ben.native
# export BEN_TEMPLATES_DIR=/srv/release.debian.org/www/transitions/bin/

all:
	umask 0002; nice -n 10 ionice -c 3 $(BEN) tracker $(OPT)

update:
	umask 0002; nice -n 10 ionice -c 3 $(BEN) tracker $(OPT) -u

cron:
# suppress output of global configuration; see bug 1096144
	umask 0002; nice -n 10 ionice -c 3 $(BEN) tracker $(OPT) -u -q 2>&1 | grep -v read_global_config || true

%.ben: FORCE
	umask 0002; $(BEN) tracker $(OPT) --no-clean -t $@

clean:
	find -iname "*.html" -delete

FORCE:
